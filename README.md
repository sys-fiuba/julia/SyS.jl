# SyS

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://sys-fiuba/julia.gitlab.io/SyS.jl/dev)
[![Build Status](https://gitlab.com/sys-fiuba/julia/SyS.jl/badges/master/pipeline.svg)](https://gitlab.com/sys-fiuba/julia/SyS.jl/pipelines)
[![Coverage](https://gitlab.com/sys-fiuba/julia/SyS.jl/badges/master/coverage.svg)](https://gitlab.com/sys-fiuba/julia/SyS.jl/commits/master)

FIUBA - SyS - Curso 1