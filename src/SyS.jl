module SyS

using Reexport
using FFTW:fft, ifft
using TestItems

export fft, ifft

@reexport using DSP
@reexport using Unitful
#@reexport using MAT
@reexport using FileIO
@reexport using JLD2
@reexport using WAV
@reexport using Random
@reexport using Statistics
@reexport using LinearAlgebra
@reexport using FFTViews
@reexport using IterTools
@reexport using LaTeXStrings
using Unitful: s, Hz, minute, hr, kHz, MHz, ms, μs
export s, Hz, minute, hr, kHz, MHz, ms, μs
using SampledSignals

export sound

using PolynomialRoots
export roots

# About plotting and playing
@reexport using Plots


export pluto
export stft, specplot, loadwav
export phase, fft, ifft, upsample, downsample, u, δ, padright
export stem, stem!
export zplane, zeropolegain, polynomialratio, getpoles, getzeroes, getgain, getnumcoefs
export serie_coefs, serie_synth

export fftshift

export xcorr_center

const fftshift = DSP.fftshift

function __init__()
    theme(:default; legend=false)
end

function pluto(;kwargs...)
    Pluto.run(; launch_browser=false, host="0.0.0.0", require_secret_for_access=false, 
              require_secret_for_open_links=false, kwargs...)
end
"""
    sound(x, sr)

Return an object that displays in a webpage as a widget that allows audio playback and downloads.

It is just calling `SampledSignals.SampleBuf(x, sr)``
"""
sound(x, sr) = SampleBuf(x, sr)



@testitem "Basic signals" begin
    @test u.(-2:2) == [0, 0, 1, 1, 1]
    @test δ.(-2:2) == [0, 0, 1, 0, 0]   
end

"""
    u(t)

Escalón.

# Examples

```jldoctest
julia> using SyS

julia> u(-1.2)
0

julia> u(0)
1

julia> u(10.5)
1

```
"""
u(t) = ifelse(t>=0, 1, 0)


"""
    δ(n)

Discrete delta.

# Examples

```jldoctest
julia> using SyS

julia> δ(-1)
0

julia> δ(0)
1

julia> δ(10)
0
```
"""
δ(n :: Integer) = ifelse( n == 0, 1, 0)
δ(t) = throw("δ is only defined for the discrete case. Make the argument an integer.")



"""
	upsample(x, n)

Upsample vector `x` by a factor of `n`.

# Examples

```jldoctest
julia> using SyS

julia> upsample([1, 2, 3], 3)
9-element Vector{Int64}:
 1
 0
 0
 2
 0
 0
 3
 0
 0
```
"""
function upsample(x::AbstractVector, n)
    out = zeros(eltype(x), n * length(x))
    out[1:n:end] .= x 

    return out
end

"""
	downsample(x, n)

Downsample vector `x` by a factor of `n`.

# Examples

```jldoctest
julia> using SyS

julia> downsample([1, 2, 3, 4, 5, 6], 3)
2-element Vector{Int64}:
 1
 4
```
"""
downsample(x::AbstractVector, n) = x[1:n:end]

"""
    phase(x)

Return the phase of vector `x` after attempting to fix its 2kπ jumps.
"""
function phase(x; range=2pi, kwargs...)
    out = DSP.unwrap(angle.(x); range, kwargs...)
    out .-= out[1] - mod(out, range)
end


#---Plotting-------------------------------------------
#------------------------------------------------------

stem(args...; kwargs...) = sticks(args...;
									marker=:circle,
									kwargs...)

stem!(args...; kwargs...) = sticks!(args...;
									marker=:circle,
									kwargs...)
for f in (:stem, :stem!)
    @eval function $f(x::Function, from::Integer, to::Integer; kwargs...)
        ns = from:to
        $f(ns, $f.(ns); kwargs...)
    end
end



DSP.filt(zpg::DSP.ZeroPoleGain, r...; kwargs...) = filt(polynomialratio(zpg), r...; kwargs...)

zeropolegain(pr) = DSP.ZeroPoleGain(pr)
zeropolegain(z, p, g) = DSP.ZeroPoleGain(z, p, g)
polynomialratio(zpg) = DSP.PolynomialRatio(zpg)
function polynomialratio(b, a)
  n = max(length(a), length(b))
  return DSP.PolynomialRatio(padright(b, n), padright(a, n))
end
getpoles(zpg) = DSP.ZeroPoleGain(zpg).p
getzeros(zpg) = DSP.ZeroPoleGain(zpg).z
getgain(zpg) = DSP.ZeroPoleGain(zpg).k
getnumcoefs(pr) = trimlastzeros!(reverse(DSP.PolynomialRatio(pr).b.coeffs))
getdencoefs(pr) = trimlastzeros!(reverse(DSP.PolynomialRatio(pr).a.coeffs))
function trimlastzeros!(a)
  !iszero(a[end]) && return a
  pop!(a)
  return trimlastzeros!(a)
end


"""
    zplane(zeros, poles; unitcircle=true, kwargs)

Make a zero-pole diagram with the complex numbers given in vectors `zeros` and `poles`.

If `unitcircle=true` (the default), then it draws the unit circle. 

It also inherits the keyword arguments of `plot`.
"""
function zplane(zs, ps; unitcircle=true, kwargs...)
	scatter(real.(zs), imag.(zs);
		  marker = (:black, :circle), label="Cero", kwargs...)
	scatter!( real.(ps), imag.(ps);
	  	marker = (:red, :xcross), label="Polo", kwargs...)
  ts = range(0,stop=2pi;length=100)
  unitcircle && plot!(cos.(ts), sin.(ts); aspect_ratio = 1, label="", kwargs...)
  plot!()
end

zplane(x; kwargs...) = zplane(getzeros(x), getpoles(x); kwargs...)

"""
    padright(x, n)

Pad vector with zeros on the right until its length is `n`
"""
padright(x, n) = copyto!(zeros(eltype(x), n), x)

"""
    cshift(t, len, from=0)

Función módulo pero con offset (opcional)
Manda a `t` al intervalo [from, from+length)
sumándole o restándole algún múltiplo de `len`
"""
cshift(t, len, from=0) = mod(t - from, len) + from

# Espectrograma
"""
    stft(x; overlap, window, nfft, rest...)
"""
function stft(x; overlap, window, nfft, rest...)
  nwin = length(window)
  @assert overlap < nwin

  res = [ fft(padright(xseg .* window, nfft))
    for xseg in partition(x, nwin, nwin - overlap)]

  return [ res[i][j] for j in 1:nfft, i in eachindex(res)]
end

"""
    specplot(x; kwargs...)

Plot a spectrogram with parameters specified by `kwargs`, as follows:

- `fs`: the sampling frequency, in Hz. Defaults to 1.
- `window`: the vector with the samples of the window, e.g. `hanning(256)`.
- `overlap`: the fraction of the window that will overlap with the next one. Defaults to 0.5. If it's an integer, it's interpreted as the number of samples to overlap.
- `nfft`: the number of points with which to compute the DFTs.
- `onesided`: if `true` it will only show positive frequencies from 0 to fs/2. If `false`, it will show from -fs/2 to fs/2. It defaults to one or the other according to `eltype(x)` being real or complex.
- It also inherits the keywords from typical plots.
"""
specplot(x::AbstractMatrix; kwargs...) = @error "You are entering a Matrix (2D Array). I need a Vector (1D Array)."
function specplot(x::AbstractVector;
      fs=1,
      onesided=false,
      xaxis="Tiempo (s)",
      yaxis="Frecuencia (Hz)",
      window=hamming(div(length(x), 16)),
      overlap=0.5,
      nfft=length(window),
      kws...)

    window isa Integer && (window = rect(window))
    overlap isa AbstractFloat && (overlap = round(Int, length(window) * overlap))

    mat = stft(x; overlap, window, nfft)

    fmax = fs
    if onesided
      mat = mat[1:div(size(mat, 1) + 2, 2), :]
      fmax = fs/2
    end

  toffset = length(window) / 2fs
  times = range(toffset; length=size(mat, 2), stop=length(x)/fs - toffset)
  freqs = range(0; length=size(mat, 1), stop=fmax)

	# Reubico las frecuencias negativas arriba de todo
  if !onesided
    freqs = cshift.(freqs, fs, -fs/2)
    ord   = sortperm(freqs)
    mat   = mat[ord, :]
    freqs = freqs[ord]
  end

	return heatmap(times, freqs, log.(abs.(mat) .+ eps());
          xaxis=xaxis, yaxis=yaxis,
          seriescolor=:bluesreds, legend=true, kws...)
 return times, freqs, mat
end

function specplot(x :: AbstractVector{<:AbstractFloat}; kws...)
    return specplot(convert.(Complex, x); onesided=true, kws...)
end



function serie_coefs(x)
	N = length(x)
	
	return [ 
		sum( x[n+1] * exp(-im * 2π * k * n / N) 
			for n in 0:N-1)
		for k in 0:N-1	
	] ./ N
end

function serie_synth(a)
	N = length(a)
	
	return [ 
		sum( a[k+1] * exp(im * 2π * k * n / N) 
			for k in 0:N-1)
		for n in 0:N-1	
	]
end

"""
    loadwav(fn)

Load a wav. Use as `x, sr = loadwav("path/to/file.wav")` to get the vector of samples and the sample rate. 
"""
function loadwav(fn::String)
	data, sr, = wavread(fn)
	data = mean(data; dims=2)[:]
	
	Float64.(data), Float64(sr)
end

end
