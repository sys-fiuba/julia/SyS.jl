using SyS
using Documenter

DocMeta.setdocmeta!(SyS, :DocTestSetup, :(using SyS); recursive=true)

makedocs(;
    modules=[SyS],
    authors="Rui Rojo <rui.rojo@gmail.com> and contributors",
    repo="https://gitlab.com/SyS/julia/SyS.jl/blob/{commit}{path}#{line}",
    sitename="SyS.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://SyS/julia.gitlab.io/SyS.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
