```@meta
CurrentModule = SyS
```

# SyS

Documentation for [SyS](https://gitlab.com/SyS/julia/SyS.jl).

```@index
```

```@autodocs
Modules = [SyS]
```
