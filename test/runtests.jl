using SyS
using Test
using Documenter

doctest(SyS)

@testitem "Down and up" begin
    @test downsample([1, 2, 3, 4, 5], 2) == [1, 3, 5]
    @test upsample([1, 2, 3], 2) == [1, 0, 2, 0, 3, 0]
end

@testitem "Convolve integer" begin
    @test conv([1, 2, 3], [0, 1, 0]) == [0, 1, 2, 3, 0]
end
